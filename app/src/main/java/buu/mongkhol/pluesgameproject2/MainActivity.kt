package buu.mongkhol.pluesgameproject2

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val QUESTION_ACTIVITY_CODE = 0
    private var trueAll = 0
    private var falseAll = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnPlus = findViewById<Button>(R.id.btnPlus)
        val btnDel = findViewById<Button>(R.id.btnDel)
        val btnAnd = findViewById<Button>(R.id.btnAnd)

        val txtTrueMain = findViewById<TextView>(R.id.txtTrueMain)
        val txtTrMain = findViewById<TextView>(R.id.txtTrMain)
        val txtFalseMain = findViewById<TextView>(R.id.txtFalseMain)
        val txtFaMain = findViewById<TextView>(R.id.txtFaMain)

        btnPlus.setBackgroundColor(Color.parseColor("#0000ff"))
        btnPlus.setTextColor(Color.parseColor("#000000"))
        btnDel.setBackgroundColor(Color.parseColor("#ffff00"))
        btnDel.setTextColor(Color.parseColor("#000000"))
        btnAnd.setBackgroundColor(Color.parseColor("#ff0000"))
        btnAnd.setTextColor(Color.parseColor("#000000"))

        txtTrMain.text = "ถูก $trueAll"
        txtFaMain.text = "ไม่ถูก $falseAll"

        btnPlus.setOnClickListener{
            val intent = Intent(MainActivity@this, plusActivity::class.java)
            startActivityForResult(intent,QUESTION_ACTIVITY_CODE)
        }
        btnDel.setOnClickListener{
            val intent = Intent(MainActivity@this, delActivity::class.java)
            startActivityForResult(intent,QUESTION_ACTIVITY_CODE)
        }
        btnAnd.setOnClickListener{
            val intent = Intent(MainActivity@this, andActivity::class.java)
            startActivityForResult(intent,QUESTION_ACTIVITY_CODE)
        }

        //------------------------------------------------------------------------------------------

        val title = findViewById<TextView>(R.id.title)
        title.setBackgroundColor(Color.parseColor("#ffccff"))
        val titleA = findViewById<TextView>(R.id.titleA)
        titleA.setBackgroundColor(Color.parseColor("#ffccff"))
        titleA.setTextColor(Color.parseColor("#9933ff"))
        val titleB = findViewById<TextView>(R.id.titleB)
        titleB.setBackgroundColor(Color.parseColor("#ffccff"))
        titleB.setTextColor(Color.parseColor("#99ffff"))
        val titleC = findViewById<TextView>(R.id.titleC)
        titleC.setBackgroundColor(Color.parseColor("#ffccff"))
        titleC.setTextColor(Color.parseColor("#6633FF"))
        val titleD = findViewById<TextView>(R.id.titleD)
        titleD.setBackgroundColor(Color.parseColor("#ffccff"))
        titleD.setTextColor(Color.parseColor("#66ff66"))
        val titleE = findViewById<TextView>(R.id.titleE)
        titleE.setBackgroundColor(Color.parseColor("#ffccff"))
        titleE.setTextColor(Color.parseColor("#00ff33"))
        val titleF = findViewById<TextView>(R.id.titleF)
        titleF.setBackgroundColor(Color.parseColor("#ffccff"))
        titleF.setTextColor(Color.parseColor("#ffff33"))
        val titleG = findViewById<TextView>(R.id.titleG)
        titleG.setBackgroundColor(Color.parseColor("#ffccff"))
        titleG.setTextColor(Color.parseColor("#ff6633"))
        val titleH = findViewById<TextView>(R.id.titleH)
        titleH.setBackgroundColor(Color.parseColor("#ffccff"))
        titleH.setTextColor(Color.parseColor("#ff0000"))
        val titleN = findViewById<TextView>(R.id.titleN)
        titleN.setBackgroundColor(Color.parseColor("#ffccff"))
        titleN.setTextColor(Color.parseColor("#0000ff"))

        //------------------------------------------------------------------------------------------

        val txtTrue = findViewById<TextView>(R.id.txtTrueMain)
        txtTrue.setTextColor(Color.parseColor("#00ff33"))
        val txtTr = findViewById<TextView>(R.id.txtTrMain)
        txtTr.setTextColor(Color.parseColor("#00ff33"))

        //------------------------------------------------------------------------------------------

        val txtFalse = findViewById<TextView>(R.id.txtFalseMain)
        txtFalse.setTextColor(Color.parseColor("#ff33cc"))
        val txtFa = findViewById<TextView>(R.id.txtFaMain)
        txtFa.setTextColor(Color.parseColor("#ff33cc"))

        //------------------------------------------------------------------------------------------


    }


    override fun onActivityResult (requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == QUESTION_ACTIVITY_CODE){
            if(resultCode == Activity.RESULT_OK){
                val returnTrue = data!!.getIntExtra("true",trueAll)
                val returnFalse = data!!.getIntExtra("false",falseAll)
                trueAll += returnTrue
                falseAll += returnFalse
                Log.e("RETURN NUMBER TRUE", returnTrue.toString())
                Log.e("RETURN NUMBER FALSE", returnFalse.toString())
                Log.e("ALL NUMBER TRUE", trueAll.toString())
                Log.e("ALL NUMBER FALSE", falseAll.toString())
                txtTrMain.text = "ถูก $trueAll"
                txtFaMain.text = "ไม่ถูก $falseAll"
            }
        }
    }

}