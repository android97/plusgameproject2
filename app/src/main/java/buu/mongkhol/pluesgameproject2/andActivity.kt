package buu.mongkhol.pluesgameproject2

import android.app.Activity
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_and.*
import kotlin.random.Random

class andActivity : AppCompatActivity() {
    private var plusCorrect:Int = 0
    private var plusIncorrect:Int = 0
    private var N1 = Random.nextInt(0, 11)
    private var N2 = Random.nextInt(0, 11)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_and)

        //------------------------------------------------------------------------------------------

        val txtTrueAnd = findViewById<TextView>(R.id.txtTrueAnd)
        txtTrueAnd.setTextColor(Color.parseColor("#00ff33"))
        val txtTrAnd = findViewById<TextView>(R.id.txtTrAnd)
        txtTrAnd.setTextColor(Color.parseColor("#00ff33"))

        //------------------------------------------------------------------------------------------

        val txtFalseAnd = findViewById<TextView>(R.id.txtFalseAnd)
        txtFalseAnd.setTextColor(Color.parseColor("#ff33cc"))
        val txtFaAnd = findViewById<TextView>(R.id.txtFaAnd)
        txtFaAnd.setTextColor(Color.parseColor("#ff33cc"))

        //------------------------------------------------------------------------------------------

        reGame()
    }

    private fun reGame() {
        N1 = Random.nextInt(0, 11)
        N2 = Random.nextInt(0, 11)
        N1 = Random.nextInt(0, 11)
        N2 = Random.nextInt(0, 11)
        val number1 = findViewById<TextView>(R.id.txtAN1)
        val number2 = findViewById<TextView>(R.id.txtAN2)

        val btn1 = findViewById<Button>(R.id.btn1)
        val btn2 = findViewById<Button>(R.id.btn2)
        val btn3 = findViewById<Button>(R.id.btn3)

        txtAN1.text = "$N1"
        txtAN2.text = "$N2"
        Log.e("Test","test")
        Log.e("num1",number1.text.toString())
        Log.e("num2",number2.text.toString())
        val answer = N1 * N2
        val answer2 = answer.toString()

        randomAnswer(btn1, btn2, btn3, answer)
        check(btn1, btn2, btn3, answer2)

        intent.putExtra("true", plusCorrect)
        intent.putExtra("false", plusIncorrect)
        setResult(Activity.RESULT_OK,intent)

    }

    private fun randomAnswer(
        btn1: Button,
        btn2: Button,
        btn3: Button,
        answer: Int
    ) {
        val ans: Int = Random.nextInt(0, 3)
        if (ans == 0) {
            btn1.text = (answer * 1).toString()
            btn2.text = ((answer * 1) + 2 ).toString()
            btn3.text = ((answer * 1) + 3 ).toString()
        }
        if (ans == 1) {
            btn1.text = ((answer * 1) - 2 ).toString()
            btn2.text = (answer * 1).toString()
            btn3.text = ((answer * 1) + 1 ).toString()
        }
        if (ans == 2) {
            btn1.text = ((answer * 1) - 1 ).toString()
            btn2.text = ((answer * 1) - 3).toString()
            btn3.text = (answer * 1).toString()
        }
    }

    private fun check(
        btn1: Button,
        btn2: Button,
        btn3: Button,
        answer2: String

    ){
        val txtTrAnd = findViewById<TextView>(R.id.txtTrAnd)
        val txtFaAnd = findViewById<TextView>(R.id.txtFaAnd)
        val txtTrueAnd = findViewById<TextView>(R.id.txtTrueAnd)
        val txtFalseAnd = findViewById<TextView>(R.id.txtFalseAnd)
        val txtAns = findViewById<TextView>(R.id.txtAns)

        btn1.setOnClickListener {
            if (answer2 == btn1.text) {
//                Toast.makeText(this, "ถูกต้อง", Toast.LENGTH_SHORT).show()
//                txtAns.text = "ถูกต้อง".toString()
                plusCorrect += 1
                txtTrAnd.text = "ถูก : $plusCorrect"
                Handler().postDelayed({
                    btn1.setBackgroundColor(Color.parseColor("#dddddd"))
                },250)
                btn1.setBackgroundColor(Color.parseColor("#33ff99"))
                reGame()
            } else {
                plusIncorrect += 1
                txtFaAnd.text = "ผิด : $plusIncorrect"
                Handler().postDelayed({
                    btn1.setBackgroundColor(Color.parseColor("#dddddd"))
                },250)
                btn1.setBackgroundColor(Color.parseColor("#ff9933"))
                reGame()
            }
        }
        btn2.setOnClickListener {
            if (answer2 == btn2.text) {
                plusCorrect += 1
                txtTrAnd.text = "ถูก : $plusCorrect"
                Handler().postDelayed({
                    btn2.setBackgroundColor(Color.parseColor("#dddddd"))
                },250)
                btn2.setBackgroundColor(Color.parseColor("#33ff99"))
                reGame()
            } else {
                plusIncorrect += 1
                txtFaAnd.text = "ผิด : $plusIncorrect"
                Handler().postDelayed({
                    btn2.setBackgroundColor(Color.parseColor("#dddddd"))
                },250)
                btn2.setBackgroundColor(Color.parseColor("#ff9933"))
                reGame()
            }
        }
        btn3.setOnClickListener {
            if (answer2 == btn3.text) {
                plusCorrect += 1
                txtTrAnd.text = "ถูก : $plusCorrect"
                Handler().postDelayed({
                    btn3.setBackgroundColor(Color.parseColor("#dddddd"))
                },250)
                btn3.setBackgroundColor(Color.parseColor("#33ff99"))
                reGame()
            } else {
                plusIncorrect += 1
                txtFaAnd.text = "ผิด : $plusIncorrect"
                Handler().postDelayed({
                    btn3.setBackgroundColor(Color.parseColor("#dddddd"))
                },250)
                btn3.setBackgroundColor(Color.parseColor("#ff9933"))
                reGame()
            }
        }

    }

}