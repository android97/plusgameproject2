package buu.mongkhol.pluesgameproject2

import android.app.Activity
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kotlin.random.Random

class delActivity : AppCompatActivity() {
    private var plusCorrect:Int = 0
    private var plusIncorrect:Int = 0
    private var N1 = Random.nextInt(0, 11)
    private var N2 = Random.nextInt(0, 11)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_del)

        //------------------------------------------------------------------------------------------

        val txtTrueDel = findViewById<TextView>(R.id.txtTrueDel)
        txtTrueDel.setTextColor(Color.parseColor("#00ff33"))
        val txtTrDel = findViewById<TextView>(R.id.txtTrDel)
        txtTrDel.setTextColor(Color.parseColor("#00ff33"))

        //------------------------------------------------------------------------------------------

        val txtFalseDel = findViewById<TextView>(R.id.txtFalseDel)
        txtFalseDel.setTextColor(Color.parseColor("#ff33cc"))
        val txtFaDel = findViewById<TextView>(R.id.txtFaDel)
        txtFaDel.setTextColor(Color.parseColor("#ff33cc"))

        //------------------------------------------------------------------------------------------

        reGame()
    }

    private fun reGame() {
        N1 = Random.nextInt(0, 11)
        N2 = Random.nextInt(0, 11)
        val N1 = Random.nextInt(6, 11)
        val N2 = Random.nextInt(0, 6)
        val number1 = findViewById<TextView>(R.id.txtDN1)
        val number2 = findViewById<TextView>(R.id.txtDN2)

        val btn1 = findViewById<Button>(R.id.btn1)
        val btn2 = findViewById<Button>(R.id.btn2)
        val btn3 = findViewById<Button>(R.id.btn3)

        number1.text = "$N1"
        number2.text = "$N2"

        val answer = N1 - N2
        val answer2 = answer.toString()

        randomAnswer(btn1, btn2, btn3, answer)
        check(btn1, btn2, btn3, answer2)

        intent.putExtra("true", plusCorrect)
        intent.putExtra("false", plusIncorrect)
        setResult(Activity.RESULT_OK,intent)

    }

    private fun randomAnswer(
        btn1: Button,
        btn2: Button,
        btn3: Button,
        answer: Int
    ) {
        val ans: Int = Random.nextInt(0, 3)
        if (ans == 0) {
            btn1.text = (answer - 0).toString()
            btn2.text = (answer + 1).toString()
            btn3.text = (answer + 2).toString()
        }
        if (ans == 1) {
            btn1.text = (answer - 1).toString()
            btn2.text = (answer - 0).toString()
            btn3.text = (answer + 2).toString()
        }
        if (ans == 2) {
            btn1.text = (answer - 2).toString()
            btn2.text = (answer + 1).toString()
            btn3.text = (answer - 0).toString()
        }
    }

    private fun check(
        btn1: Button,
        btn2: Button,
        btn3: Button,
        answer2: String

    ){
        val txtTrDel = findViewById<TextView>(R.id.txtTrDel)
        val txtFaDel = findViewById<TextView>(R.id.txtFaDel)
        val txtTrueDel = findViewById<TextView>(R.id.txtTrueDel)
        val txtFalseDel = findViewById<TextView>(R.id.txtFalseDel)
        val txtAns = findViewById<TextView>(R.id.txtAns)

        btn1.setOnClickListener {
            if (answer2 == btn1.text) {
                plusCorrect += 1
                txtTrDel.text = "ถูก : $plusCorrect"
                Handler().postDelayed({
                    btn1.setBackgroundColor(Color.parseColor("#dddddd"))
                    reGame()
                },250)
                btn1.setBackgroundColor(Color.parseColor("#33ff99"))

            } else {
                plusIncorrect += 1
                txtFaDel.text = "ผิด : $plusIncorrect"
                Handler().postDelayed({
                    btn1.setBackgroundColor(Color.parseColor("#dddddd"))
                    reGame()
                },250)
                btn1.setBackgroundColor(Color.parseColor("#ff9933"))

            }
        }
        btn2.setOnClickListener {
            if (answer2 == btn2.text) {
                plusCorrect += 1
                txtTrDel.text = "ถูก : $plusCorrect"
                Handler().postDelayed({
                    btn2.setBackgroundColor(Color.parseColor("#dddddd"))
                    reGame()
                },250)
                btn2.setBackgroundColor(Color.parseColor("#33ff99"))

            } else {
                plusIncorrect += 1
                txtFaDel.text = "ผิด : $plusIncorrect"
                Handler().postDelayed({
                    btn2.setBackgroundColor(Color.parseColor("#dddddd"))
                    reGame()
                },250)
                btn2.setBackgroundColor(Color.parseColor("#ff9933"))

            }
        }
        btn3.setOnClickListener {
            if (answer2 == btn3.text) {
                plusCorrect += 1
                txtTrDel.text = "ถูก : $plusCorrect"
                Handler().postDelayed({
                    btn3.setBackgroundColor(Color.parseColor("#dddddd"))
                    reGame()
                },250)
                btn3.setBackgroundColor(Color.parseColor("#33ff99"))

            } else {
                plusIncorrect += 1
                txtFaDel.text = "ผิด : $plusIncorrect"
                Handler().postDelayed({
                    btn3.setBackgroundColor(Color.parseColor("#dddddd"))
                    reGame()
                },250)
                btn3.setBackgroundColor(Color.parseColor("#ff9933"))

            }
        }

    }

}